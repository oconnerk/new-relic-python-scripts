import os
import json
import requests
import argparse


def read_config(file_path):
    """Read configuration from a JSON file."""
    with open(file_path, "r") as f:
        return json.load(f)


def fetch_dashboard(api_endpoint, headers, dashboard_id):
    """
    Fetch the dashboard JSON structure using a GraphQL query.
    This query retrieves each widget's title and rawConfiguration.
    """
    query = f"""\
{{
  actor {{
    entity(guid: "{dashboard_id}") {{
      ... on DashboardEntity {{
        guid
        name
        pages {{
          name
          widgets {{
            title
            rawConfiguration
          }}
        }}
      }}
    }}
  }}
}}
"""
    response = requests.post(api_endpoint, json={"query": query}, headers=headers)
    if response.status_code == 200:
        return response.json()
    else:
        print("Error fetching dashboard: {} - {}".format(response.status_code, response.text))
        return None


def clone_dashboard_data(original_dashboard):
    """
    Clone and modify the dashboard data for creation.
    Builds a new dashboard dictionary without the original GUID and prefixes the name with "Cloned: ".
    Each widget's configuration is extracted from its rawConfiguration.
    """
    return {
        "name": "Cloned: " + original_dashboard.get("name", "Untitled Dashboard"),
        "pages": [
            {
                "name": page.get("name", "Untitled Page"),
                "widgets": [
                    {
                        "title": widget.get("title", "Untitled Widget"),
                        "rawConfiguration": widget.get("rawConfiguration", "{}")
                    }
                    for widget in page.get("widgets", [])
                ]
            }
            for page in original_dashboard.get("pages", [])
        ]
    }


def create_dashboard(api_endpoint, headers, new_dashboard):
    """
    Create a new dashboard using the dashboardCreate mutation.
    The widget configuration is passed as a JSON string in the rawConfiguration field.
    """
    mutation = """\
mutation($dashboard: DashboardInput!) {
  dashboardCreate(dashboard: $dashboard) {
    errors {
      description
    }
    dashboard {
      guid
      name
      pages {
        name
        widgets {
          title
          rawConfiguration
        }
      }
    }
  }
}
"""
    variables = {"dashboard": new_dashboard}
    response = requests.post(api_endpoint, json={"query": mutation, "variables": variables}, headers=headers)
    if response.status_code == 200:
        return response.json()
    else:
        print("Error creating dashboard: {} - {}".format(response.status_code, response.text))
        return None


def main():
    # Parse command-line argument for the source dashboard GUID.
    parser = argparse.ArgumentParser(description="Clone a New Relic Dashboard")
    parser.add_argument("source_dashboard_guid", help="GUID of the source dashboard to clone")
    args = parser.parse_args()
    source_dashboard_guid = args.source_dashboard_guid

    # Read configurations from external JSON file.
    config_path = os.path.join(os.path.dirname(__file__), "config.json")
    config = read_config(config_path)
    api_endpoint = config["API_ENDPOINT"]  # e.g., "https://api.newrelic.com/graphql"
    api_key = config["API_KEY"]

    headers = {
        "Content-Type": "application/json",
        "API-Key": api_key
    }

    # Fetch the original dashboard JSON structure.
    fetched_data = fetch_dashboard(api_endpoint, headers, source_dashboard_guid)
    if not fetched_data:
        print("Failed to fetch dashboard data.")
        return

    original_dashboard = fetched_data.get("data", {}).get("actor", {}).get("entity", {})
    print("Fetched Dashboard JSON:")
    print(json.dumps(original_dashboard, indent=2))

    # Clone and modify the dashboard properties.
    new_dashboard = clone_dashboard_data(original_dashboard)

    # Create a new dashboard with the modified data.
    creation_result = create_dashboard(api_endpoint, headers, new_dashboard)
    if creation_result:
        print("New Dashboard Creation Result:")
        print(json.dumps(creation_result, indent=2))
    else:
        print("Dashboard creation failed.")


if __name__ == "__main__":
    main()
