# New Relic Cost Reporting Scripts

Scripts to get cost information from New Relic for the past month (ending now).

Note: Only 'getNRCosts.py' is ready for use at this time.

## Installation

1. Get the scripts and 'config.json' to a host with Python 3 installed.
2. Edit 'config.json'
    - Add your API Key
    - Change the 'service_group_mappings' for your applications

Python packages required:
- argparse
- concurrent.futures
- json
- os
- time
- from collections import defaultdict
- requests

Notes:

1. During script execution, the service names as defined in the service_groups_mapping are appended with a wildcard character. For example, "LicenseMgmt" will match all services beginning with this string.
2. A warning is generated when running this on MacOS (possibly other Linux OS's). It's possible to get rid of this warning by downgrading urllib3 to < 2.0, but I've decided to live with it.
   -  NotOpenSSLWarning: urllib3 v2.0 only supports OpenSSL 1.1.1+, currently the 'ssl' module is compiled with 'LibreSSL 2.8.3'.

## Possible Enhancements
1. Specify the reporting period as a script argument.

## Usage

```python

# returns cost information for Acquisitions services in dev
getNRCosts.py --env Dev --service-group ACQ

# returns cost information for MSI services in all environments
getNRCosts.py --env all --service-group MSI

```
