#!/Users/oconnerk/PycharmProjects/NewRelicCosts/venv/bin/python3

# Import necessary modules
import argparse
import concurrent.futures
import json
import os
import time
from collections import defaultdict

import requests

# Function to read configuration from a JSON file
def read_config(file_path):
    with open(file_path, 'r') as f:
        return json.load(f)

# Read configurations from external JSON file
config_path = os.path.join(os.path.dirname(__file__), 'config.json')
config = read_config(config_path)

# Use the read configurations
API_ENDPOINT = config['API_ENDPOINT']
API_KEY = config['API_KEY']
ACCOUNT_ID = config['ACCOUNT_ID']
service_group_mappings = config['service_group_mappings']  # Read from config
all_environments = config['all_environments']  # Read from config
datacenters_dev_nit_nat = config['datacenters_dev_nit_nat']  # Read from config
datacenters_prod = config['datacenters_prod']  # Read from config

HEADERS = {
    'Content-Type': 'application/json',
    'API-Key': API_KEY
}

# NRQL queries for different storage types
queries = {
    'Events': '''SELECT bytecountestimate() / 1e9 * 0.30 AS '$' FROM Transaction, TransactionError WHERE appName LIKE '{}%-{}-{}' LIMIT MAX SINCE 1 month ago''',
    'Metrics': '''SELECT bytecountestimate() / 1e9 * 0.30 AS '$' FROM Metric WHERE newrelic.source = 'agent' AND instrumentation.provider NOT IN ('kentik', 'pixie') AND appName LIKE '{}%-{}-{}' LIMIT MAX SINCE 1 month ago''',
    'Tracing': '''SELECT bytecountestimate() / 1e9 * 0.30 AS '$' FROM Span, ErrorTrace, SqlTrace WHERE instrumentation.provider != 'pixie' AND appName LIKE '{}%-{}-{}' LIMIT MAX SINCE 1 month ago''',
    'Logging': '''SELECT bytecountestimate() / 1e9 * 0.30 AS '$' FROM Log, LogExtendedRecord WHERE instrumentation.provider != 'kentik' AND entity.name LIKE '{}%-{}-{}' LIMIT MAX SINCE 1 month ago'''
}

# Retrying configuration for API limits
MAX_RETRIES = 3
RETRY_BACKOFF = 5  # in seconds

# Updated command-line argument parsing
available_service_groups = list(service_group_mappings.keys())
parser = argparse.ArgumentParser(description='Query New Relic for storage costs.')
parser.add_argument('--service-group', required=True, type=str, choices=available_service_groups + ['all'], help='Service group name')
parser.add_argument('--env', required=True, type=str, choices=['Dev', 'NIT', 'NAT', 'Prod', 'all'], help='Environment name')
args = parser.parse_args()

# Filter services and environments based on command-line arguments
if args.service_group == 'all':
    services = [service for group in available_service_groups for service in service_group_mappings[group]]
else:
    services = service_group_mappings[args.service_group]
environments = [args.env] if args.env != 'all' else all_environments

# Aggregation structures for final outputs
totals_services = {service: 0 for service in services}
totals_environments = {env: 0 for env in environments}
grand_total = 0

# Storage for intermediate results
results_storage = defaultdict(lambda: defaultdict(lambda: defaultdict(float)))

# Function to execute a specific query for a service, environment, and datacenter
def execute_query(service, env, dc, query_type, query):
    formatted_query = query.format(service, env, dc)
    payload = {
        "query": f'''{{
            actor {{
                nrql(
                    accounts: [{ACCOUNT_ID}],
                    options: {{}},
                    query: "{formatted_query}",
                    timeout: 60
                ) {{
                    results
                }}
            }}
        }}'''
    }

    retries = 0
    while retries < MAX_RETRIES:
        response = requests.post(API_ENDPOINT, headers=HEADERS, data=json.dumps(payload))

        if 'errors' in response.json() and response.json()['errors'][0]['extensions']['errorClass'] == 'TOO_MANY_REQUESTS':
            sleep_duration = RETRY_BACKOFF * (retries + 1)
            time.sleep(sleep_duration)
            retries += 1
            continue

        data = response.json()
        if 'errors' in data:
            return service, env, dc, query_type, None

        results = data['data']['actor']['nrql']['results']
        cost = round(results[0]['$'], 2)

        return service, env, dc, query_type, cost

    if retries == MAX_RETRIES:
        return service, env, dc, query_type, None

# Parallel execution of queries using threads
with concurrent.futures.ThreadPoolExecutor() as executor:
    futures = []
    for service in sorted(services):
        for env in environments:
            if env in ["Dev", "NIT", "NAT"]:
                datacenters = datacenters_dev_nit_nat
            else:
                datacenters = datacenters_prod

            for dc in datacenters:
                for query_type, query in sorted(queries.items()):
                    futures.append(executor.submit(execute_query, service, env, dc, query_type, query))

    for future in concurrent.futures.as_completed(futures):
        service, env, dc, query_type, cost = future.result()
        if cost is not None:
            totals_services[service] += cost
            totals_environments[env] += cost
            grand_total += cost
            results_storage[service][env + "-" + dc][query_type] = cost

# Printing results
for service in sorted(services):
    for env in environments:
        if env in ["Dev", "NIT", "NAT"]:
            datacenters = datacenters_dev_nit_nat
        else:
            datacenters = datacenters_prod

        for dc in datacenters:
            for query_type in sorted(queries.keys()):
                cost = results_storage[service][env + "-" + dc][query_type]
                print(f"Storage cost for {query_type} in {service}-{env}-{dc}: ${cost:.2f}")

print("\nTotals for all services across all environments:")
for service, cost in totals_services.items():
    print(f"{service}: ${cost:.2f}")

print("\nTotals for all environments across all services:")
for env, cost in totals_environments.items():
    print(f"{env}: ${cost:.2f}")

print(f"\nGrand Total: ${grand_total:.2f}")
