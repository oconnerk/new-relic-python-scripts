#!/Users/oconnerk/PycharmProjects/NewRelicCosts/venv/bin/python3

# Import necessary modules
import json
import os
import requests
import time

# Function to read configuration from a JSON file
def read_config(file_path):
    with open(file_path, 'r') as f:
        return json.load(f)

# Read configurations from external JSON file
config_path = os.path.join(os.path.dirname(__file__), 'config.json')
config = read_config(config_path)

# Use the read configurations
API_ENDPOINT = config['API_ENDPOINT']
API_KEY = config['API_KEY']
ACCOUNT_ID = config['ACCOUNT_ID']

HEADERS = {
    'Content-Type': 'application/json',
    'API-Key': API_KEY
}

# NRQL query for Logging storage type
logging_query = '''SELECT bytecountestimate() / 1e9 * 0.30 AS '$' FROM Log, LogExtendedRecord WHERE cluster_name = 'k8s-devops-prod' and namespace_name = 'kube-system' or namespace_name = 'cattle-monitoring-system' LIMIT MAX SINCE 1 month ago'''

# Retrying configuration for API limits
MAX_RETRIES = 3
RETRY_BACKOFF = 5  # in seconds

# Function to execute the specific Logging query
def execute_logging_query():
    payload = {
        "query": f'''{{
            actor {{
                nrql(
                    accounts: [{ACCOUNT_ID}],
                    options: {{}},
                    query: "{logging_query}",
                    timeout: 60
                ) {{
                    results
                }}
            }}
        }}'''
    }

    retries = 0
    while retries < MAX_RETRIES:
        response = requests.post(API_ENDPOINT, headers=HEADERS, data=json.dumps(payload))

        if 'errors' in response.json() and response.json()['errors'][0]['extensions'][
            'errorClass'] == 'TOO_MANY_REQUESTS':
            sleep_duration = RETRY_BACKOFF * (retries + 1)
            time.sleep(sleep_duration)
            retries += 1
            continue

        data = response.json()
        if 'errors' in data:
            return None

        results = data['data']['actor']['nrql']['results']
        cost = round(results[0]['$'], 2)

        return cost

    if retries == MAX_RETRIES:
        return None

# Execute the Logging query and print the result
logging_cost = execute_logging_query()
if logging_cost is not None:
    print(f"Storage cost for Logging in KUBERNETES k8s-devops-prod cluster: ${logging_cost:.2f}")
else:
    print("Failed to retrieve the cost information.")
