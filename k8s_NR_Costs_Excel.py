# Import necessary modules
import json
import os
import requests
import time
import pandas as pd  # For data manipulation
from openpyxl import load_workbook
from openpyxl.chart import LineChart, Reference  # Import LineChart


# Function to read configuration from a JSON file
def read_config(file_path):
    with open(file_path, 'r') as f:
        return json.load(f)


# Read configurations from external JSON file
config_path = os.path.join(os.path.dirname(__file__), 'config.json')
config = read_config(config_path)

# Use the read configurations
API_ENDPOINT = config['API_ENDPOINT']
API_KEY = config['API_KEY']
ACCOUNT_ID = config['ACCOUNT_ID']

HEADERS = {
    'Content-Type': 'application/json',
    'API-Key': API_KEY
}

# NRQL query for Logging storage type with 'FACET namespace_name'
logging_query = '''SELECT bytecountestimate() / 1e9 * 0.30 AS '$' FROM Log, LogExtendedRecord WHERE cluster_name = 'k8s-devops-prod' FACET namespace_name LIMIT MAX SINCE 1 MONTH ago TIMESERIES 1 WEEK'''


# Function to execute the specific Logging query
def execute_logging_query():
    payload = {
        "query": f'''{{
            actor {{
                nrql(
                    accounts: [{ACCOUNT_ID}],
                    options: {{}},
                    query: "{logging_query}",
                    timeout: 120
                ) {{
                    results
                }}
            }}
        }}'''
    }

    retries = 0
    MAX_RETRIES = 5
    RETRY_BACKOFF = 5  # in seconds

    while retries < MAX_RETRIES:
        response = requests.post(API_ENDPOINT, headers=HEADERS, data=json.dumps(payload))
        response_data = response.json()

        if 'errors' in response_data:
            error_class = response_data['errors'][0]['extensions']['errorClass']

            if error_class == 'TOO_MANY_REQUESTS':
                print(f"Error: Too many requests. Retrying in {RETRY_BACKOFF * (retries + 1)} seconds.")
                sleep_duration = RETRY_BACKOFF * (retries + 1)
                time.sleep(sleep_duration)
                retries += 1
                continue
            elif error_class == 'TIMEOUT':
                print(f"Error: Timeout. Retrying in {RETRY_BACKOFF * (retries + 1)} seconds.")
                sleep_duration = RETRY_BACKOFF * (retries + 1)
                time.sleep(sleep_duration)
                retries += 1
                continue
            elif error_class == 'SERVER_ERROR':
                print(f"Error: Server error. Retrying in {RETRY_BACKOFF * (retries + 1)} seconds.")
                sleep_duration = RETRY_BACKOFF * (retries + 1)
                time.sleep(sleep_duration)
                retries += 1
                continue
            else:
                print(f"Error: {error_class}")
                return None

        results = response_data['data']['actor']['nrql']['results']
        return results

    if retries == MAX_RETRIES:
        print("Max retries reached. Exiting.")
        return None


# Execute the Logging query and print the result
logging_costs = execute_logging_query()

if logging_costs is not None:
    # Prepare data for Excel and plotting
    df_data = {}
    print("Storage cost for Logging in KUBERNETES k8s-devops-prod cluster:")

    for record in logging_costs:
        namespace = record['namespace_name']
        cost = round(record['$'], 2)
        week_start = time.strftime('%Y-%W', time.localtime(record['beginTimeSeconds']))

        if week_start not in df_data:
            df_data[week_start] = {}

        if namespace not in df_data[week_start]:
            df_data[week_start][namespace] = 0.0

        df_data[week_start][namespace] += cost

        print(f"  Namespace {namespace} for week starting {week_start}: added ${cost:.2f}")

    # Create a DataFrame
    df = pd.DataFrame(df_data).fillna(0)
    df.index.name = 'Namespace'

    # Write to Excel
    excel_path = 'logging_costs.xlsx'
    df.to_excel(excel_path)

    # Embed the line chart in Excel
    wb = load_workbook(excel_path)
    ws = wb.active

    chart = LineChart()
    chart.title = "Weekly Cost per Namespace"
    chart.y_axis.title = 'Cost'
    chart.x_axis.title = 'Week'

    data = Reference(ws, min_col=2, min_row=1, max_row=ws.max_row, max_col=ws.max_column)
    categories = Reference(ws, min_col=2, max_col=ws.max_column, min_row=1, max_row=1)  # Changed this line
    chart.add_data(data, titles_from_data=True)
    chart.set_categories(categories)

    ws.add_chart(chart, "D5")

    wb.save(excel_path)

    print(f"Excel file with embedded line chart has been saved to {excel_path}")
else:
    print("Failed to retrieve the cost information.")
